package FindNeighbour2D

object Direction extends Enumeration
{
  type Direction = Value
  val NORTH,
      SOUTH,
      EAST,
      WEST,
      NORTHEAST,
      NORTHWEST,
      SOUTHEAST,
      SOUTHWEST = Value
}
