package FindNeighbour2D

object FindNeighbour2D {
  /**
   * Translates coordinates into an object contained in the list
   * @param list The actual list
   * @param coordinates Coordinates to translate
   * @param width width of one row
   * @tparam Any The type of the objects contained in the list
   * @return the object located at those coordinates x, y
   */
  private def coordinatesToObject[Any](list: List[Any], coordinates : Vector2i, width: Int): Any = {
    list(coordinates.y * width + coordinates.x)
  }

  /**
   * Translates coordinates into an index
   * @param coordinates coordinates to translate
   * @param width Width of one row
   * @return The index for those coordinates x, y
   */
  def coordinatesToIndex(coordinates : Vector2i, width: Int): Int = {
    coordinates.y * width + coordinates.x
  }

  /**
   * Finds the coordinates from the index.
   * @param index The index to find the coordinates for
   * @param width The width of the grid
   * @return An array containing coordinates (x, y)
   */
  def indexToCoordinates(index: Int, width: Int) : Vector2i = {
    // root = 4, width = 5
    val result : Vector2i = new Vector2i()
    val x : Int = index % width
    val y : Int = index / width
    result.x = x
    result.y = y
    result
  }

  def FindNeighbourInDirection[Any](objects : List[Any],
                                   coordinates : Vector2i,
                                   width : Int,
                                   height : Int,
                                   direction : Direction.Direction,
                                   wrapAround : Boolean) : Int = {
    if (coordinatesToIndex(coordinates, width) < 0
      || coordinatesToIndex(coordinates, width) >= width * height)
      -1

    findNeighbourAt(objects, coordinates, width, width * height, direction, wrapAround)
  }

  /**
    * Finds the neighbours of the root tile
    * @param objectsList A list containing all objects
    * @param coordinates x, y coordinates
    * @param width width of one row
    * @param height height of one column
    * @param wrapAround does the search wraps around the "map"?
    * @param diagonals find neighbours in diagonal
    * @tparam Any -
    * @return A map containing entries in the form of "[Cardinal direction]" - Index in list. Ex: "NORTH" - 5
    */
  def FindAllNeighbours[Any](objectsList: List[Any],
                         coordinates : Vector2i,
                         width: Int,
                         height: Int,
                         wrapAround: Boolean = false,
                         diagonals: Boolean = false): Map[String, Int] = {
    var result = Map[String, Int]()

    if (coordinatesToIndex(coordinates, width) < 0 || coordinatesToIndex(coordinates, width) >= width * height) {
      result += "NORTH" -> -1
      return result
    }

    val north = findNeighbourAt(objectsList, coordinates, width, width * height, Direction.NORTH, wrapAround)
    if (north != -1)
      result += "NORTH" -> north

    val east = findNeighbourAt(objectsList, coordinates, width, width * height, Direction.EAST, wrapAround)
    if (east != -1)
      result += "EAST" -> east

    val west = findNeighbourAt(objectsList, coordinates, width, width * height, Direction.WEST, wrapAround)
    if (west != -1)
      result += "WEST" -> west

    val south = findNeighbourAt(objectsList, coordinates, width, width * height, Direction.SOUTH, wrapAround)
    if (south != -1)
      result += "SOUTH" -> south

    if(diagonals)
      {
        val northEast = findNeighbourAt(objectsList, coordinates, width, width * height, Direction.NORTHEAST, wrapAround)
        if (northEast != -1)
          result += "NORTHEAST" -> northEast

        val northWest = findNeighbourAt(objectsList, coordinates, width, width * height, Direction.NORTHWEST, wrapAround)
        if (northWest != -1)
          result += "NORTHWEST" -> northWest

        val southEast = findNeighbourAt(objectsList, coordinates, width, width * height, Direction.SOUTHEAST, wrapAround)
        if (southEast != -1)
          result += "SOUTHEAST" -> southEast

        val southWest = findNeighbourAt(objectsList, coordinates, width, width * height, Direction.SOUTHWEST, wrapAround)
        if (southWest != -1)
          result += "SOUTHWEST" -> southWest
      }

    result
  }

  private def findNeighbourAt[A >: Null](
                                          list: List[Any],
                                          coordinates : Vector2i,
                                          width: Int,
                                          totalSize: Int,
                                          direction: Direction.Value,
                                          wrapAround: Boolean): Int = {
    val rootIndex = coordinatesToIndex(coordinates, width)

    direction match {
      case Direction.NORTH =>
        val target = rootIndex - width
        if (target < 0 && !wrapAround)
          -1
        else if (target < 0 && wrapAround)
          totalSize + target
        else
          rootIndex - width

      case Direction.SOUTH =>
        val target = rootIndex + width
        if (target >= totalSize && !wrapAround)
          -1
        else if (rootIndex + width >= totalSize && wrapAround)
          target % width
        else
          target

      case Direction.WEST =>
        val target = rootIndex - 1
        if (target % width <= 0 && !wrapAround)
          -1
        else if (target % width <= 0 && wrapAround)
          target + width
        else
          target

      case Direction.EAST =>
        val target : Int = rootIndex + 1
        if (target % width == 0 && !wrapAround)
          -1
        else if (target % width == 0 && wrapAround)
          rootIndex - width + 1
        else
          rootIndex + 1

      case Direction.NORTHEAST =>
        val target = rootIndex - width + 1
        if (target < 0 && !wrapAround)
            -1
        else if(target < 0 && wrapAround)
            totalSize + target
        else
          if (target % width != 0 && target - width > 0)
            target
          else if(target % width == 0 && target - width > 0)
            target - width
          else
            totalSize - width


      case Direction.NORTHWEST =>
        val target = rootIndex - width - 1
        if (target < 0 && ! wrapAround)
          -1
        else if(target < 0 && wrapAround)
          totalSize - 1
        else
          target

      case Direction.SOUTHEAST =>
        val target = rootIndex + width + 1
        if (target >= totalSize && !wrapAround)
          -1
        else if (target >= totalSize && wrapAround)
          0
        else
          target


      case Direction.SOUTHWEST =>
        val target = rootIndex + width - 1
        if(target >= totalSize && ! wrapAround)
          -1
        else if(target >= totalSize - 1 && wrapAround)
          width - 1
        else
          target

      case anythingElse => -1
    }
  }
}