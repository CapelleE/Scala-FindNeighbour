package FindNeighbour2D

class Vector2i(pX : Int = 0, pY : Int = 0)
{
  var x : Int = pX
  var y : Int = pY

  override def equals(obj: scala.Any): Boolean = {
    obj match {
      case theObj: Vector2i =>
        theObj.x == theObj.x && theObj.y == theObj.y

      case _ => false
    }
  }

  override def toString: String = {
    "(x=%d, y=%d)".format(x, y)
  }
}
