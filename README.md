Find the 2D neighbours of a "Tile", given a 1D array.

Currently supports:
* Toggleable "Wrap around" (ability to "get around the map" when hitting an edge)
* Toggleable diagonals (search for diagonals too, or not)
* The source array can contain any type of object (Integer, Floats, custom objects, ...)

Includes tests

Tools used:
* Scala

Todo:
* Test more & debug if needed