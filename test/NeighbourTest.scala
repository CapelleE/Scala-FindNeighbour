import FindNeighbour2D.{Direction, Vector2i, FindNeighbour2D}
import org.junit.Test
import org.junit.Assert._

class NeighbourTest {
  val values: List[Int] = List(
    0,  1,  2,  3,  4,
    5,  6,  7,  8,  9,
    10, 11, 12, 13, 14,
    15, 16, 17, 18, 19,
    20, 21, 22, 23, 24
  )
  val width: Int = 5
  val height: Int = 5

  @Test
  def testCoordinatesToIndex(): Unit = {
    val coords = new Vector2i(2, 3)
    val expected = 17
    val indexFound = FindNeighbour2D.coordinatesToIndex(coords, width)
    assertEquals("Coordinates to index test failed: expected %d, got %d".format(expected, indexFound), expected, indexFound)
    println("Coordinates to Index test success.")
  }

  @Test
  def testIndexToCoordinates(): Unit = {
    val rootIndex = 4
    val expected: Vector2i = new Vector2i(4, 0)
    val coordsFound = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    assertEquals("Index to coords test failed: expected (%d, %d), got (%d, %d)".format(expected.x, expected.y,
      coordsFound.x, coordsFound.y), expected, coordsFound)
    println("Index to coords test success.")
  }

  @Test
  def testNorthEdge(): Unit = {
    // RootIndex = 0, 0 (0)
    val rootIndex: Int = 0
    val expected: Int = 20
    val coords = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    var edge: Int = 0
    try {
      edge = FindNeighbour2D.FindNeighbourInDirection(values, coords, width, height, Direction.NORTH, wrapAround = true)
      assertEquals("North edge test failed, expected %d, got %d".format(expected, edge), expected, edge)
      println("North edge test success")
    }
    catch {
      case nsee: NoSuchElementException =>
        println("North edge not found!")
    }
  }

  @Test
  def testEastEdge(): Unit = {
    val rootIndex: Int = 24
    val coords = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    val expected: Int = 20
    var edge: Int = 0
    try {
      edge = FindNeighbour2D.FindNeighbourInDirection(values, coords, width, height, Direction.EAST, wrapAround = true)
      assertEquals("East edge test failed, expected %d, got %d".format(expected, edge), expected, edge)
      println("East edge test success")
    }
    catch {
      case nsee: NoSuchElementException =>
        println("East edge not found!")
    }
  }

  @Test
  def testSouthEdge(): Unit = {
    val rootIndex: Int = 24
    val coords = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    val expected: Int = 4
    var edge: Int = 0
    try {
      edge = FindNeighbour2D.FindNeighbourInDirection(values, coords, width, height, Direction.SOUTH, wrapAround = true)
      assertEquals("South edge test failed: expected %d, got %d".format(expected, edge), expected, edge)
      println("South edge test success")
    }
    catch {
      case nsee: NoSuchElementException =>
        println("South edge not found!")
    }
  }

  @Test
  def testWestEdge(): Unit = {
    val rootIndex: Int = 0
    val coords = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    val expected: Int = 4
    var edge: Int = 0
    try {
      edge = FindNeighbour2D.FindNeighbourInDirection(values, coords, width, height, Direction.WEST, wrapAround = true)
      assertEquals("West edge test failed: expected %d, got %d".format(expected, edge), expected, edge)
      println("West edge test success")
    }
    catch {
      case nsee: NoSuchElementException =>
        println("West edge not found!")
    }
  }

  @Test
  def testNorthEastEdge() : Unit = {
    val rootIndex : Int = 4
    val expected : Int = 20
    val coords = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    var edge : Int = 0
    try {
      edge = FindNeighbour2D.FindNeighbourInDirection(values, coords, width, height,
        Direction.NORTHEAST, wrapAround = true)
      assertEquals("North-East edge test failed, expected %d, got %d".format(expected, edge), expected, edge)
      println("North-east edge test success.")
    }
    catch
      {
        case nsee : NoSuchElementException =>
          println("North east edge not found.")
      }
  }

  @Test
  def testNorthWestEdge() : Unit = {
    val rootIndex : Int = 6
    val expected : Int = 0
    val coords = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    var edge : Int = 0
    try {
      edge = FindNeighbour2D.FindNeighbourInDirection(values, coords, width, height,
        Direction.NORTHWEST, wrapAround = true)
      assertEquals("North-west edge test failed, expected %d, got %d".format(expected, edge), expected, edge)
      println("North-west edge test success.")
    }
    catch
      {
        case nsee : NoSuchElementException =>
          println("North west edge not found.")
      }
  }

  @Test
  def testSouthEastEdge() : Unit = {
    val rootIndex : Int = 24
    val expected : Int = 0
    val coords = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    var edge : Int = 0
    try {
      edge = FindNeighbour2D.FindNeighbourInDirection(values, coords, width, height,
        Direction.SOUTHEAST, wrapAround = true)
      assertEquals("South east edge test failed, expected %d, got %d".format(expected, edge), expected, edge)
      println("South east edge test success.")
    }
    catch
      {
        case nsee : NoSuchElementException =>
          println("South east edge not found.")
      }
  }

  @Test
  def testSouthWestEdge() : Unit = {
    val rootIndex : Int = 20
    val expected : Int = 4
    val coords = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    var edge : Int = 0
    try {
      edge = FindNeighbour2D.FindNeighbourInDirection(values, coords, width, height,
        Direction.SOUTHWEST, wrapAround = true)
      assertEquals("South west edge test failed, expected %d, got %d".format(expected, edge), expected, edge)
      println("South west edge test success.")
    }
    catch
      {
        case nsee : NoSuchElementException =>
          println("South west edge not found.")
      }
  }

  @Test
  def testMiddleNeighbours() : Unit = {
    /*
    6  7  8
    11 ro 13
    16 17 18
     */
    val rootIndex : Int = 12
    val expected : Map[String, Int] = Map[String, Int](
    "NORTHWEST" -> 6,
    "NORTH" -> 7,
    "NORTHEAST" -> 8,
    "WEST" -> 11,
    "EAST" -> 13,
    "SOUTHWEST" -> 16,
    "SOUTH" -> 17,
    "SOUTHEAST" -> 18
    )
    val coords : Vector2i = FindNeighbour2D.indexToCoordinates(rootIndex, width)
    val found = FindNeighbour2D.FindAllNeighbours(values, coords, width, height, wrapAround = true, diagonals = true)
    try
    {
      assert(found == expected)
      println("Test middle neighbours success.")
    }
    catch
      {
        case ae : AssertionError =>
          println("Test middle neighbours failed!")
          println("Differences:")
          expected.foreach(f =>
          {
            if(expected(f._1) != found(f._1))
              {
                println("Expected: %d, got: %d at key %s".format(f._2, found(f._1), f._1))
              }
          })
      }
  }
}
